package utils;

import controller.ParticipantDataController;
import controller.ProgramDataController;
import controller.SearchQPController;
import controller.SkillsController;
import javafx.scene.CacheHint;
import javafx.scene.layout.VBox;

public class Controllers {

	private static ProgramDataController programDataController;
	private static ParticipantDataController participantDataController;
	private static SkillsController skillsController;
	private static SearchQPController searchQPController;
	
	public static void getProgramData(VBox box) {
		programDataController = programDataController == null ? new ProgramDataController() : programDataController ;
		config(box, programDataController);
	}
	
	public static void getParticipantData(VBox box) {
		participantDataController = participantDataController == null ? new ParticipantDataController() : participantDataController ;
		config(box, participantDataController);
	}
	
	public static void getSkills(VBox box) {
		skillsController = skillsController == null ? new SkillsController() : skillsController ;
		config(box, skillsController);
	}
	
	public static void getSearchQP(VBox box) {
		searchQPController = searchQPController == null ? new SearchQPController() : searchQPController ;
		config(box, searchQPController);
	}
	
	//since we will open view by clicking on the Main Menu buttons we need to clear the existing views
	//and open the new ones
	//this method here does that
	//clears the existing view and adds the new view
	public static void config(VBox box, VBox content) {
		box.getChildren().clear();
		box.getChildren().add(content);
		box.setCache(true);
		box.setCacheShape(true);
		box.setCacheHint(CacheHint.SPEED);
	}

}
