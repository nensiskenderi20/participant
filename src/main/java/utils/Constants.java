package utils;

import java.util.ArrayList;
import java.util.List;

public class Constants {

	public static final String b1 =  "BUSINESS";
	public static final String b2 =  "HEALTH CARE";
	public static final String b3 =  "BACHELOR";
	public static final String b4 =  "COMPUTER SCIENCE";
	public static final String b5 =  "TEST";
	public static final String b6 =  "PROGRAMMING";
	public static final String b7 =  "JAVA";
	public static final String b8 =  "JAVAFX";
	public static final String b9 =  "MYSQL";
	public static final String b10 =  "MSSQL";
	public static final String b11 =  "DATA";
	public static final String b12 =  "DATABASE";
	
	public static List<String> searchMainMenu = new ArrayList<String>(){{
	    add(b1); add(b2); add(b3);  add(b4);  add(b5); add(b6); add(b7); add(b8);
	    add(b9);add(b10); add(b11); add(b12);
	}};
}
