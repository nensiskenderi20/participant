package utils;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.jfoenix.controls.JFXAlert;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextField;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

//this is a class that contains helper methods we will use a lot
//we add them here to avoid code duplicates
public class HelperMethods {

	public static void closeStage(JFXButton button) {
		Stage stage = (Stage) button.getScene().getWindow();
		stage.close();
	}

	public static void styleDeleteButton(Button btn_delete) {
		btn_delete.setMaxWidth(20);
		btn_delete.setCursor(Cursor.HAND);
		btn_delete.getStyleClass().add("delete");
	}
	
	public static void deleteAlert(JFXAlert alert,String entity,Button confirm, Button cancel) {
		alert.initModality(Modality.APPLICATION_MODAL);
		alert.setOverlayClose(false);
		JFXDialogLayout layout = new JFXDialogLayout();
		ImageView img = new ImageView(new Image("/images/warning.png"));
		Label warning = new Label("  Warning! ");
		warning.setStyle("-fx-text-fill: #4A86E8;-fx-cursor: hand;");
		HBox.setMargin(warning, new Insets(5, 0, 0, 0));
		HBox hbox = new HBox();
		hbox.getChildren().addAll(img, warning);
		layout.setHeading(hbox);
		layout.setBody(new Label(entity));
		layout.setActions(confirm, cancel);
		alert.setContent(layout);
		alert.show();
	}
	
	public static void style_delete_button(JFXButton btn_delete) {
		btn_delete.setMaxWidth(20);
		btn_delete.setCursor(Cursor.HAND);
		btn_delete.getStyleClass().add("delete");
	}

	public void openEditScene(String view_name) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/"+view_name+".fxml")); 
		Parent root=(Parent)loader.load();
		Scene scene = new Scene(root);
		scene.setFill(Color.TRANSPARENT);
		Stage stage = new Stage();
		stage.initStyle(StageStyle.TRANSPARENT); 
		stage.setScene(scene);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.showAndWait();
	}
	
	public void openScene(String view_name) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/"+view_name+".fxml")); 
		Parent root=(Parent)loader.load();
		Scene scene = new Scene(root);
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.showAndWait();
	}


	public static void alerti(String title, String text, AlertType type) {
		Alert alert = new Alert (type,text);
		alert.setTitle(title);
		alert.setHeaderText(null);
		alert.showAndWait();
	}

	public static boolean checkEmptyText(String... strings) {
		for(String s : strings)
			if(s == null || s.isEmpty())
				return true;

		return false;
	}

	public static void make_textfield_integer(TextField txt) {
		txt.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,12}?")) {
					txt.setText(oldValue);
				}
			}
		});
	}

	public static void make_skill_integer(TextField txt) {
		txt.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,4}?")) {
					txt.setText(oldValue);
				}
			}
		});
	}
	
	public static void refreshFocusTable(TableView<?> tbl) {
		tbl.refresh();
		tbl.requestFocus();
	}
	
	public static void clearTexts(TextField...textFields) {
		for(TextField t : textFields) 
			t.setText("");
	}
}
