package utils;

import javafx.scene.control.TextFormatter;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;

public class TextSearchBuilder {

	//here we build the customizable text search
	public static void build_txtSearch(AutoCompleteTextField txtSearch, HBox hbox_search_holder) {
		txtSearch.setTextFormatter(new TextFormatter<>((change) -> {
		    change.setText(change.getText().toUpperCase());
		    return change;
		}));
		txtSearch.getEntries().addAll(Constants.searchMainMenu);
		txtSearch.setPromptText("Search Bar");
		txtSearch.setPrefWidth(170);
		txtSearch.setPrefHeight(50);
		txtSearch.setMaxHeight(HBox.USE_PREF_SIZE);
		txtSearch.setMinHeight(HBox.USE_PREF_SIZE);
		txtSearch.setFocusColor(Paint.valueOf("#4A86E8"));
		txtSearch.setFont(Font.font(20));
		HBox.setHgrow(txtSearch, Priority.ALWAYS);
		hbox_search_holder.getChildren().clear();
		hbox_search_holder.getChildren().add(txtSearch);
	}
}
