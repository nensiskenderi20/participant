package dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Program;

public class ProgramDao extends DAO {
	public ProgramDao() {
		super();
	}
	
	public List<Program> getPrograms() throws SQLException {
		List<Program> data = new ArrayList<Program>();
		String query = "select program_id, program_name, program_type, program_credential, "
				+ "program_description from participants.program ";
		stm = connector.prepareStatement(query);
		rs = stm.executeQuery(query); 

		while(rs.next()) {
			Program program = new Program();
			program.setProgram_id(rs.getInt(1));
			program.setProgram_name(rs.getString(2));
			program.setProgram_type(rs.getString(3));
			program.setProgram_credential(rs.getString(4));
			program.setProgram_description(rs.getString(5));
			
			data.add(program);
		}
		return data;
	}
	
	public void addProgram(Program program) throws SQLException {
		String insert = "INSERT INTO participants.program " + 
				"(program_name, program_type, program_credential, program_description) VALUES (?,?,?,?)";
		stm = connector.prepareStatement(insert);

		stm.setString(1, program.getProgram_name());
		stm.setString(2, program.getProgram_type());
		stm.setString(3, program.getProgram_credential());
		stm.setString(4, program.getProgram_description());
		
		stm.executeUpdate();
		stm.close();
	}

	public void updateProgram(Program program) throws SQLException {
		String update = "UPDATE participants.program SET program_name = ?, program_type = ?, program_credential = ?, "
				+ "program_description = ? WHERE program_id = ?";
		stm = connector.prepareStatement(update);

		stm.setString(1, program.getProgram_name());
		stm.setString(2, program.getProgram_type());
		stm.setString(3, program.getProgram_credential());
		stm.setString(4, program.getProgram_description());
		
		stm.setInt(5, program.getProgram_id());
		

		stm.executeUpdate();
		stm.close();
	}

	public void deleteProgram(int id) throws SQLException {
		String query = "DELETE FROM participants.program where program_id = ?";
		stm = connector.prepareStatement(query);
		stm.setInt(1, id);

		stm.execute();
		stm.close();
		
	}
	
//	public int getIdFromBoja(String emri) throws Exception {
//		String sql_query = "select id,emri from toner.bojra where emri = '"+emri+"'";
//		stm = connector.prepareStatement(sql_query);
//		rs = stm.executeQuery(sql_query);
//		
//		while(rs.next()) 
//			return rs.getInt(1);
//		return 0;
//	}
//	
//	public boolean checkBojaInventar(int bojaId) throws SQLException {
//		String query = "select * from toner.inventari where bojra_id = '"+bojaId+"'";
//		stm = connector.prepareStatement(query);
//		rs = stm.executeQuery();
//		if(rs.next() == true)
//			return true;
//		else 
//			return false;
//	}
}
