package dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Skills;


public class SkillDao extends DAO {

	public List<Skills> getSkillDao() throws SQLException {
		List<Skills> data = new ArrayList<Skills>();
		String query = "select skill_id, skill_description, skill_rank from participants.skills ";
		stm = connector.prepareStatement(query);
		rs = stm.executeQuery(query); 

		while(rs.next()) {
			Skills skill = new Skills();
			skill.setSkill_id(rs.getInt(1));
			skill.setSkill_description(rs.getString(2));
			skill.setSkill_rank(rs.getString(3));
			
			data.add(skill);
		}
		return data;
	}
	
	public void addSkill(Skills skill) throws SQLException {
		String insert = "INSERT INTO participants.skills " + 
				"(skill_id, skill_description, skill_rank) VALUES (?,?,?)";
		stm = connector.prepareStatement(insert);

		stm.setInt(1, skill.getSkill_id());
		stm.setString(2, skill.getSkill_description());
		stm.setString(3, skill.getSkill_rank());
		
		stm.executeUpdate();
		stm.close();
	}

	public void updateSkill(Skills skill) throws SQLException {
		String update = "UPDATE participants.skills SET skill_description = ?, skill_rank = ? WHERE skill_id = ?";
		stm = connector.prepareStatement(update);

		stm.setString(1, skill.getSkill_description());
		stm.setString(2, skill.getSkill_rank());
		stm.setInt(3, skill.getSkill_id());

		stm.executeUpdate();
		stm.close();
	}

	public void deleteSkill(int id) throws SQLException {
		String query = "DELETE FROM participants.skills where skill_id = ?";
		stm = connector.prepareStatement(query);
		stm.setInt(1, id);

		stm.execute();
		stm.close();
		
	}
	
}
