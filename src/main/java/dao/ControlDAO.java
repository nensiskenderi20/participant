package dao;

public class ControlDAO {
	//this is a general class to handle all of our database classes
	private static ControlDAO dao = new ControlDAO();
	private ProgramDao programDao = new ProgramDao();
	private ParticipantDataDao participantDao = new ParticipantDataDao();
	private SkillDao skillDao = new SkillDao();
	private SearchQPDao searchQPDao = new SearchQPDao();
	
	public static ControlDAO getControlDao() {
		return dao;
	}
	
	public ProgramDao getProgramDao() {
		return programDao;
	}
	
	public ParticipantDataDao getParticipantDao() {
		return participantDao;
	}
	
	public SkillDao getSkillDao() {
		return skillDao;
	}
	
	public SearchQPDao getSearchQPDao() {
		return searchQPDao;
	}
	
}
