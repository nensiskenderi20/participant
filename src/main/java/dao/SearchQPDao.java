package dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Participant;
import model.PersonalInfo;


public class SearchQPDao extends DAO {

	public SearchQPDao() {
		super();
	}
	
	public List<Participant> viewQualifiedParticipants() throws SQLException{
		//we get all the qualified participants by making a join in 2 tables 
		//we are able to get all the data we need from the 2 tables
		List<Participant> data = new ArrayList<Participant>();
		String query = "SELECT p.participant_id, pi.first_name, "
				+ "pi.middle_name, pi.last_name, p.gpa, p.participant_resume FROM participants.personal_info "
				+ "pi join participants.participant p on pi.personal_info_id = p.personal_info_id";
		stm = connector.prepareStatement(query);
		rs = stm.executeQuery(query);

		while(rs.next()) {
			PersonalInfo pi = new PersonalInfo();
			Participant p = new Participant();
			p.setParticipant_id(rs.getInt(1));
			pi.setFirst_name(rs.getString(2));
			pi.setMiddle_name(rs.getString(3));
			pi.setLast_name(rs.getString(4));
			p.setGpa(rs.getDouble(5));
			p.setParticipant_resume(rs.getString(6));
			p.setPersonal_info_id(pi);

			data.add(p);
		}
		//here we return that data because we need to fill the table view with this list
		return data;
	}

}
