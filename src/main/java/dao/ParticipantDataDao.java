package dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.PersonalInfo;


	public class ParticipantDataDao extends DAO {

		public ParticipantDataDao() {
			super();
	}
	
	public void addParticipantData(PersonalInfo participant) throws SQLException {
		
		//we add the participant data by entering the info values we got from our frontend view
		String insert_participant = "INSERT INTO participants.personal_info " + 
				"(personal_info_id, first_name, middle_name, last_name, date_of_birth, gender, ethnicity,"
				+ "phone_number, street, apt_number, city, state, zip_code, department, email) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		stm = connector.prepareStatement(insert_participant);

		stm.setInt(1, participant.getPersonal_info_id());
		stm.setString(2, participant.getFirst_name());
		stm.setString(3, participant.getMiddle_name());
		stm.setString(4, participant.getLast_name());
		stm.setDate(5, participant.getDate_of_birth());
		stm.setString(6, participant.getGender());
		stm.setString(7, participant.getEthnicity());
		stm.setString(8, participant.getPhone_number());
		stm.setString(9, participant.getStreet());
		stm.setInt(10, participant.getApt_number());
		stm.setString(11, participant.getCity());
		stm.setString(12, participant.getState());
		stm.setInt(13, participant.getZip_code());
		stm.setString(14, participant.getDepartment());
		stm.setString(15, participant.getEmail());

		stm.executeUpdate();
		stm.close();
		}
	
	public List<PersonalInfo> viewParticipantData() throws SQLException{
		//here we create a list of personal data which we fetch from the database table
		//we fill this list and add it to the table view
		List<PersonalInfo> data = new ArrayList<PersonalInfo>();
		String query = "SELECT personal_info_id, first_name, middle_name, last_name, date_of_birth, gender, ethnicity, "
				+ "phone_number, street, apt_number, city, state, zip_code, department, email FROM participants.personal_info";
		stm = connector.prepareStatement(query);
		rs = stm.executeQuery(query);
		
		while(rs.next()) {
			PersonalInfo p = new PersonalInfo();
			p.setPersonal_info_id(rs.getInt(1));
			p.setFirst_name(rs.getString(2));
			p.setMiddle_name(rs.getString(3));
			p.setLast_name(rs.getString(4));
			p.setDate_of_birth(rs.getDate(5));
			p.setGender(rs.getString(6));
			p.setEthnicity(rs.getString(7));
			p.setPhone_number(rs.getString(8));
			p.setStreet(rs.getString(9));
			p.setApt_number(rs.getInt(10));
			p.setCity(rs.getString(11));
			p.setState(rs.getString(12));
			p.setZip_code(rs.getInt(13));
			p.setDepartment(rs.getString(14));
			p.setEmail(rs.getString(15));
			
			data.add(p);
		}
		return data;
	}
	
	
	public void deleteParticipantData(int personal_info_id) throws SQLException{
		//here we create the delete function by taking the id and delete the row in the database
		String query = "DELETE FROM participants.personal_info WHERE personal_info_id=?";
		stm = connector.prepareStatement(query);
		stm.setInt(1, personal_info_id);
		
		stm.execute();
		stm.close();
		
	}
	
	public void updateParticipantData(PersonalInfo p) throws SQLException {
		//we update a specific participant by getting their values from the p object
		String update = "UPDATE participants.personal_info SET  first_name = ?,"
				+ "middle_name = ?, last_name = ?, date_of_birth = ?, gender = ?, ethnicity = ?,"
				+ "phone_number = ?, street = ?, apt_number = ?, city = ?, state = ?, "
				+ "zip_code = ?, department = ?, email = ? WHERE personal_info_id = ?";
		stm = connector.prepareStatement(update);

		
		stm.setString(1, p.getFirst_name());
		stm.setString(2, p.getMiddle_name());
		stm.setString(3, p.getLast_name());
		stm.setDate(4, p.getDate_of_birth());
		stm.setString(5, p.getGender());
		stm.setString(6, p.getEthnicity());
		stm.setString(7, p.getPhone_number());
		stm.setString(8, p.getStreet());
		stm.setInt(9, p.getApt_number());
		stm.setString(10, p.getCity());
		stm.setString(11, p.getState());
		stm.setInt(12, p.getZip_code());
		stm.setString(13, p.getDepartment());
		stm.setString(14, p.getEmail());
		stm.setInt(15, p.getPersonal_info_id());
		
		
		stm.executeUpdate();
		stm.close();
		
	}
	
}
