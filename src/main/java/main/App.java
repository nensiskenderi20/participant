package main;

import java.io.InputStream;
import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class App extends Application {

	public Parent root;
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		final Thread thread = Thread.currentThread();
		if (thread == null) {
			throw new AssertionError("Current thread is null!");
		}
		final ClassLoader classLoader = thread.getContextClassLoader();
		if (classLoader == null) {
			throw new AssertionError("Current classloader is null!");
		}

		final String fxmlName = "views/MainMenu.fxml";
		URL url = classLoader.getResource(fxmlName);
		if (url == null) {
			throw new AssertionError(fxmlName + " not found!");
		}

		FXMLLoader loader = new FXMLLoader(url);
		root = (Parent) loader.load();

		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Main Menu");

		final String imageName = "images/logo.png";
		final InputStream is = classLoader.getResourceAsStream(imageName);
		if (is == null) {
			throw new AssertionError("Could not load image: " + imageName);
		}

		primaryStage.getIcons().add(new Image(is));
		primaryStage.show();
	}


	public static void main(String[] args) {
		launch(args);
	}
}
