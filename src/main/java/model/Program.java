package model;

public class Program {

	private int program_id;
	private String program_name, program_type, program_credential, program_description;
	
	public Program() {}
	
	public Program(int program_id, String program_name, String program_type, String program_credential,
			String program_description) {
		this.program_id = program_id;
		this.program_name = program_name;
		this.program_type = program_type;
		this.program_credential = program_credential;
		this.program_description = program_description;
	}

	public Program(String program_name, String program_type, String program_credential, String program_description) {
		this.program_name = program_name;
		this.program_type = program_type;
		this.program_credential = program_credential;
		this.program_description = program_description;
	}

	public int getProgram_id() {
		return program_id;
	}

	public void setProgram_id(int program_id) {
		this.program_id = program_id;
	}

	public String getProgram_name() {
		return program_name;
	}

	public void setProgram_name(String program_name) {
		this.program_name = program_name;
	}

	public String getProgram_type() {
		return program_type;
	}

	public void setProgram_type(String program_type) {
		this.program_type = program_type;
	}

	public String getProgram_credential() {
		return program_credential;
	}

	public void setProgram_credential(String program_credential) {
		this.program_credential = program_credential;
	}

	public String getProgram_description() {
		return program_description;
	}

	public void setProgram_description(String program_description) {
		this.program_description = program_description;
	}
	
}
