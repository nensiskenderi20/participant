package model;

public class Skills {

	private int skill_id;
	private String skill_description, skill_rank;
	
	
	
	public Skills() {
		super();
	}

	public Skills(int skill_id, String skill_description, String skill_rank) {
		this.skill_id = skill_id;
		this.skill_description = skill_description;
		this.skill_rank = skill_rank;
	}

	public Skills(String skill_description, String skill_rank) {
		this.skill_description = skill_description;
		this.skill_rank = skill_rank;
	}

	public int getSkill_id() {
		return skill_id;
	}

	public void setSkill_id(int skill_id) {
		this.skill_id = skill_id;
	}

	public String getSkill_description() {
		return skill_description;
	}

	public void setSkill_description(String skill_description) {
		this.skill_description = skill_description;
	}

	public String getSkill_rank() {
		return skill_rank;
	}

	public void setSkill_rank(String skill_rank) {
		this.skill_rank = skill_rank;
	}
	
}
