package model;

import java.sql.Date;

public class PersonalInfo {
	
	private int personal_info_id, apt_number, zip_code;
	
	private String first_name, middle_name, last_name, gender,
	ethnicity, phone_number, street, city, state, department, email;
	
	private Date date_of_birth;

	public PersonalInfo(int personal_info_id, int apt_number, int zip_code, String first_name, String middle_name,
			String last_name, String gender, String ethnicity, String phone_number, String street, String city,
			String state, String department, Date date_of_birth, String email) {
		super();
		this.personal_info_id = personal_info_id;
		this.apt_number = apt_number;
		this.zip_code = zip_code;
		this.first_name = first_name;
		this.middle_name = middle_name;
		this.last_name = last_name;
		this.gender = gender;
		this.ethnicity = ethnicity;
		this.phone_number = phone_number;
		this.street = street;
		this.city = city;
		this.state = state;
		this.department = department;
		this.date_of_birth = date_of_birth;
		this.email = email;
	}

	public PersonalInfo() {
		super();
	}

	public int getPersonal_info_id() {
		return personal_info_id;
	}

	public void setPersonal_info_id(int personal_info_id) {
		this.personal_info_id = personal_info_id;
	}

	public int getApt_number() {
		return apt_number;
	}

	public void setApt_number(int apt_number) {
		this.apt_number = apt_number;
	}

	public int getZip_code() {
		return zip_code;
	}

	public void setZip_code(int zip_code) {
		this.zip_code = zip_code;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEthnicity() {
		return ethnicity;
	}

	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Date getDate_of_birth() {
		return date_of_birth;
	}

	public void setDate_of_birth(Date date_of_birth) {
		this.date_of_birth = date_of_birth;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
