package model;

import java.sql.Date;

public class Participant {
	
	private int participant_id;
	
	private PersonalInfo personal_info_id;
	
	private double gpa;
	
	private Date participant_job_status_date;
	
	private String participant_resume, participant_job_status, transcript1, transcript2;

	public Participant(int participant_id, PersonalInfo personal_info_id, double gpa,
			Date participant_job_status_date, String participant_resume, String participant_job_status,
			String transcript1, String transcript2) {
		super();
		this.participant_id = participant_id;
		this.personal_info_id = personal_info_id;
		this.gpa = gpa;
		this.participant_job_status_date = participant_job_status_date;
		this.participant_resume = participant_resume;
		this.participant_job_status = participant_job_status;
		this.transcript1 = transcript1;
		this.transcript2 = transcript2;
	}

	public Participant() {
		super();
	}

	public int getParticipant_id() {
		return participant_id;
	}

	public void setParticipant_id(int participant_id) {
		this.participant_id = participant_id;
	}

	public PersonalInfo getPersonal_info_id() {
		return personal_info_id;
	}

	public void setPersonal_info_id(PersonalInfo personal_info_id) {
		this.personal_info_id = personal_info_id;
	}

	public double getGpa() {
		return gpa;
	}

	public void setGpa(double gpa) {
		this.gpa = gpa;
	}

	public Date getParticipant_job_status_date() {
		return participant_job_status_date;
	}

	public void setParticipant_job_status_date(Date participant_job_status_date) {
		this.participant_job_status_date = participant_job_status_date;
	}

	public String getParticipant_resume() {
		return participant_resume;
	}

	public void setParticipant_resume(String participant_resume) {
		this.participant_resume = participant_resume;
	}

	public String getParticipant_job_status() {
		return participant_job_status;
	}

	public void setParticipant_job_status(String participant_job_status) {
		this.participant_job_status = participant_job_status;
	}

	public String getTranscript1() {
		return transcript1;
	}

	public void setTranscript1(String transcript1) {
		this.transcript1 = transcript1;
	}

	public String getTranscript2() {
		return transcript2;
	}

	public void setTranscript2(String transcript2) {
		this.transcript2 = transcript2;
	}

	
	
}