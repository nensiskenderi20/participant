package controller;

import java.io.IOException;
import java.sql.SQLException;

import com.jfoenix.controls.JFXAlert;
import com.jfoenix.controls.JFXButton;

import dao.ControlDAO;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Skills;
import utils.HelperMethods;

public class SkillsController extends VBox {

	@FXML
	private TableView<Skills> tblSkills;

	@FXML
	private TableColumn<Skills, String> tblcolSkillId, tblcolSkillDescription, tblcolSkillRank;

	@FXML
	private TableColumn<Skills, Skills> tblcolDelete;

	@FXML
	private TextField txtAddSkill, txtAddSkillRank, txtEditSkill, txtEditSkillRank;

	@FXML
	private JFXButton saveAdd, btnSave;
	
	private int skill_id = 0;

	private ObservableList<Skills> tableViewData = FXCollections.observableArrayList();
	
	public SkillsController() {
		FXMLLoader fxml = new FXMLLoader(getClass().getResource("/views/Skills.fxml"));

		fxml.setRoot(this);
		fxml.setController(this);
		try {
			fxml.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void initialize() throws SQLException {
		//this is the first method that will be called when loading this view
		//here we make the ranks textfields to accept only numbers (for add and edit too)
		HelperMethods.make_skill_integer(txtAddSkillRank);
		HelperMethods.make_skill_integer(txtEditSkillRank);
		//we need to call click_item here because this method will be used everytime user double clicks on a row
		click_item();
		//here we load the table view with the data we have from the database
		loadSkill();
	}

	private void loadSkill() throws SQLException {
		//since the table view data will change on every add/edit we need te clear it because otherwise it will display the data multiple times
		tblSkills.getItems().clear();
		//we get the data from the database using the database layer with classes on dao package
		tableViewData.addAll(ControlDAO.getControlDao().getSkillDao().getSkillDao());

		//we set every column in the table view to match the respective table column in the database
		tblcolSkillId.setCellValueFactory(new PropertyValueFactory<>("skill_id"));
		tblcolSkillDescription.setCellValueFactory(new PropertyValueFactory<>("skill_description"));
		tblcolSkillRank.setCellValueFactory(new PropertyValueFactory<>("skill_rank"));
		
		//here we set some style for the delete button
		tblcolDelete.setStyle("-fx-alignment:center;");
		tblcolDelete.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
		tblcolDelete.setCellFactory(param -> new TableCell<Skills, Skills>() {

			Button delete = new Button("");

			protected void updateItem(Skills f, boolean empty) {
				super.updateItem(f, empty);

				if (f == null) {
					setGraphic(null);
					return;
				}

				setGraphic(delete);
				HelperMethods.styleDeleteButton(delete);

				delete.setOnMouseClicked(event -> {
					//on delete we display an alert to pop up and to ask the user if he wants to delete the skill he choose or not
					JFXAlert alert = new JFXAlert((Stage) tblSkills.getScene().getWindow());
					JFXButton cancel = new JFXButton("Cancel");
					//we set some styling to the buttons
					cancel.setStyle("-fx-background-color: #DA251E; -fx-text-fill: white;-fx-cursor: hand;");
					JFXButton confirm = new JFXButton("Confirm");
					confirm.setStyle("-fx-background-color: #4A86E8; -fx-text-fill: white;-fx-cursor: hand;");
					HelperMethods.deleteAlert(alert, "Are you sure you want to delete?", confirm, cancel);
					confirm.setOnAction(e -> {
						//if user confirms that he wants to delete then we delete by taking the row id
						delete(f.getSkill_id());
						//then we close the alert
						alert.close();
					});
					cancel.setOnAction(e1 -> {
						//if he changes his mind we close the alert
						alert.close();
					});
					//we refresh the table because of the changes we made (if user deletes a row)
					HelperMethods.refreshFocusTable(tblSkills);
				});
			}
		});

		//then we populate the table view with data that we have taken from the database
		tblSkills.setItems(tableViewData);
	}

	@FXML
	private void saveAdd() throws SQLException {
		//in order to add a new skill, we create a new Skill object
		Skills skill = new Skills();
		//we fill this object from data that the user fills in the textfields
		skill.setSkill_id(skill_id);
		skill.setSkill_description(txtAddSkill.getText());
		skill.setSkill_rank(txtAddSkillRank.getText());
		//we add this new skill by using the database layer again
		ControlDAO.getControlDao().getSkillDao().addSkill(skill);
		//afterwards we clear all the textfields so user can add a new skill again
		HelperMethods.clearTexts(txtAddSkill, txtAddSkillRank, txtEditSkill, txtEditSkillRank);
		//since we added new row, we have to load the table again
		loadSkill();
	}

	
	@FXML
	private void saveEdit() throws SQLException {
		//here we want to edit a new skill by getting their values from the double click action 
		//which is the click_item method we called in the beginning
			s.setSkill_rank(txtEditSkillRank.getText());
			s.setSkill_description(txtEditSkill.getText());
			//we update this skill and store it in the database
			ControlDAO.getControlDao().getSkillDao().updateSkill(s);
			//the table needs to be loaded again because we did changes
			loadSkill();
			// we clear the texts so we can add or edit again
			HelperMethods.clearTexts(txtAddSkill, txtAddSkillRank, txtEditSkill, txtEditSkillRank);
			btnSave.setDisable(true);
		
	}
	
	
	private void delete(int skill_id) {
		try {
			//we delete a specific row by getting the skill id
			ControlDAO.getControlDao().getSkillDao().deleteSkill(skill_id);
			loadSkill();
			HelperMethods.clearTexts(txtAddSkill, txtAddSkillRank, txtEditSkill, txtEditSkillRank);
			btnSave.setDisable(true);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	Skills s = new Skills();
	
	private void click_item() {
		tblSkills.setOnMousePressed(new EventHandler<MouseEvent>() {
		    @Override 
		    public void handle(MouseEvent event) {
		    	//here we check if the user clicked twice 
		        if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
		            if (tblSkills.getSelectionModel().getSelectedItem() == null) 
		            	btnSave.setDisable(true);
					else {
						//if so, we are able to get the data we want from the table view
						btnSave.setDisable(false);
						Skills skillForEdit = tblSkills.getSelectionModel().getSelectedItem();
						//and use this data to fill the textfieds
						s.setSkill_id(skillForEdit.getSkill_id());
						txtEditSkill.setText(skillForEdit.getSkill_description());
						txtEditSkillRank.setText(skillForEdit.getSkill_rank());
					}               
		        }
		    }
		});
	}
}
