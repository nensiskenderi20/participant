package controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import javafx.scene.layout.AnchorPane;
import model.Program;
import utils.HelperMethods;

public class AddProgramController implements Initializable{

	@FXML
	private Label lblError;

	@FXML
	private JFXButton btnCancel;

	@FXML
	private AnchorPane anchorPane;

	@FXML
	private JFXTextArea txtAreaDescription;

	@FXML
	private AnchorPane programName;

	@FXML
	private AnchorPane programType;

	@FXML
	private AnchorPane programCredentials;

	//here we create choices like menu buttons in order t be able to choose more than one chioce from the list
	final MenuButton choicesProgramName = new MenuButton("*-Choose Program Names-");
	final MenuButton choicesProgramType = new MenuButton("*-Choose Program Types-");
	final MenuButton choicesProgramCredentials = new MenuButton("*-Choose Program Credentials-");

	//we fill them with data provided (may change later)
	final List<CheckMenuItem > programNameItems = Arrays.asList( 
			new CheckMenuItem("CNC Manufacturing") ,
			new CheckMenuItem("Maritime Electrical") ,
			new CheckMenuItem ("Maritime Pipefitting") ,
			new CheckMenuItem ("Plumbing Apprenticeship"),
			new CheckMenuItem("Dental Assistant") ,
			new CheckMenuItem("Dental Radiography") ,
			new CheckMenuItem ("Pharmacy Technician II") ,
			new CheckMenuItem ("Mortgage Data Processor"),
			new CheckMenuItem("Women in Tech") ,
			new CheckMenuItem("Software Developer Program") ,
			new CheckMenuItem ("Securities Industry Essentials") ,
			new CheckMenuItem ("Data Literacy Course") 
			);


	final List<CheckMenuItem > programTypeItems = Arrays.asList( 
			new CheckMenuItem("Manufacturing and Trades") ,
			new CheckMenuItem("Healthcare") ,
			new CheckMenuItem ("Education") ,
			new CheckMenuItem ("Business and Technology")
			);


	final List<CheckMenuItem > programCredentialsItems = Arrays.asList( 
			new CheckMenuItem("Electrical Apprenticeship certificate") ,
			new CheckMenuItem("Certificate in Advanced Manufacturing") ,
			new CheckMenuItem ("DNAB certification I") ,
			new CheckMenuItem ("DNAB certification II")
			);

	private int programId = 0;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		//since this is the first method we call, we need to fill and the choices so they are displayed to the user
		choicesProgramName.getItems().addAll(programNameItems);
		choicesProgramType.getItems().addAll(programTypeItems);
		choicesProgramCredentials.getItems().addAll(programCredentialsItems);

		//we add all the choices to thei respective anchor panes that we declared in the view
		programName.getChildren().add(choicesProgramName);
		programType.getChildren().add(choicesProgramType);
		programCredentials.getChildren().add(choicesProgramCredentials);

		if(ProgramDataController.edit == true) {
			programId = ProgramDataController.programDataHolder.getProgram_id();
			getData(ProgramDataController.programDataHolder);
		}
		else
			programId = 0;

		for (final CheckMenuItem item: programNameItems)  { 
			item.selectedProperty().addListener((observableValue, oldValue, newValue ) -> { 
				if (newValue)
					selectedItems.getItems().add(item.getText()) ; 
				else  
					selectedItems.getItems().remove(item.getText()); 
			}); 
		}
	}

	//the logic of this controller is not finished yet, it will be on later prototypes
	private void getData(Program program) {
		//		cmbProgramName.setValue(program.getProgram_name());
		//		cmbProgramType.setValue(program.getProgram_type());
		//		cmbProgramCredentials.setValue(program.getProgram_credential());
		txtAreaDescription.setText(program.getProgram_description());
	}

	@FXML
	private void cancel() {
		HelperMethods.closeStage(btnCancel);
	}

	@FXML
	private void save() throws SQLException {
		//		if(HelperMethods.checkEmptyCombobox(cmbProgramName, cmbProgramType, cmbProgramCredentials)) {
		//			lblError.setText("Fill the required fields!*");
		//			return;
		//		}
		//		Program program = new Program();
		//		program.setProgram_id(programId);
		//		program.setProgram_name(cmbProgramName.getValue().toString());
		//		program.setProgram_type(cmbProgramType.getValue().toString());
		//		program.setProgram_credential(cmbProgramCredentials.getValue().toString());
		//		program.setProgram_description(txtAreaDescription.getText().toString());
		//		
		//		if(programId == 0)
		//			ControlDAO.getControlDao().getProgramDao().addProgram(program);
		//		else
		//			ControlDAO.getControlDao().getProgramDao().updateProgram(program);
		//		
		//		HelperMethods.closeStage(btnCancel);

		System.out.println(selectedItems.getItems().toString());
	}

	final ListView<String> selectedItems = new ListView<>() ;
}
