package controller;

import java.io.IOException;
import java.sql.SQLException;

import com.jfoenix.controls.JFXAlert;
import com.jfoenix.controls.JFXButton;

import dao.ControlDAO;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.PersonalInfo;
import model.Program;
import utils.HelperMethods;

public class ParticipantDataController extends VBox {

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnEdit;

    @FXML
    private TableView<PersonalInfo> tblParticipantData;

    @FXML
    private TableColumn<PersonalInfo, String> tblcolParticipantId, tblcolFirstName, tblcolMiddleName, tblcolLastName, tblcolBirthdate, 
    tblcolGender, tblcolPhoneNumber, tblcolEthnicity;
    
    private ObservableList<PersonalInfo> tableViewData = FXCollections.observableArrayList();
	public static boolean edit = false;
	public static PersonalInfo participantDataHolder = new PersonalInfo();


    @FXML
    private TableColumn<PersonalInfo, PersonalInfo> tblcolDelete;
    
    public ParticipantDataController() {
    	FXMLLoader fxml = new FXMLLoader(getClass().getResource("/views/ParticipantData.fxml"));

		fxml.setRoot(this);
		fxml.setController(this);
		try {
			fxml.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    @FXML
    public void initialize() throws SQLException {
    	loadParticipant();
    }

    private void loadParticipant() throws SQLException {
		tblParticipantData.getItems().clear();
		tableViewData.addAll(ControlDAO.getControlDao().getParticipantDao().viewParticipantData());
		
		tblcolParticipantId.setCellValueFactory(new PropertyValueFactory<>("personal_info_id"));
		tblcolFirstName.setCellValueFactory(new PropertyValueFactory<>("first_name"));
		tblcolMiddleName.setCellValueFactory(new PropertyValueFactory<>("middle_name"));
		tblcolLastName.setCellValueFactory(new PropertyValueFactory<>("last_name"));
		tblcolBirthdate.setCellValueFactory(new PropertyValueFactory<>("date_of_birth"));
		tblcolGender.setCellValueFactory(new PropertyValueFactory<>("gender"));
		tblcolPhoneNumber.setCellValueFactory(new PropertyValueFactory<>("phone_number"));
		tblcolEthnicity.setCellValueFactory(new PropertyValueFactory<>("ethnicity"));
		
		tblcolDelete.setStyle("-fx-alignment:center;");
		tblcolDelete.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
		tblcolDelete.setCellFactory(param -> new TableCell<PersonalInfo, PersonalInfo>() {

			Button delete = new Button("");

			protected void updateItem(PersonalInfo f, boolean empty) {
				super.updateItem(f, empty);

				if (f == null) {
					setGraphic(null);
					return;
				}

				setGraphic(delete);
				HelperMethods.styleDeleteButton(delete);

				delete.setOnMouseClicked(event -> {
					JFXAlert alert = new JFXAlert((Stage) tblParticipantData.getScene().getWindow());
					JFXButton cancel = new JFXButton("Cancel");
					cancel.setStyle("-fx-background-color: #DA251E; -fx-text-fill: white;-fx-cursor: hand;");
					JFXButton confirm = new JFXButton("Confirm");
					confirm.setStyle("-fx-background-color: #4A86E8; -fx-text-fill: white;-fx-cursor: hand;");
					HelperMethods.deleteAlert(alert, "Are you sure you want to delete?", confirm, cancel);
					confirm.setOnAction(e -> {
						delete(f.getPersonal_info_id());
						alert.close();
					});
					cancel.setOnAction(e1 -> {
						alert.close();
					});
					HelperMethods.refreshFocusTable(tblParticipantData);
				});
			}
		});

		tblParticipantData.setItems(tableViewData);
	}
    
    private void getData() throws IOException, SQLException {
    	PersonalInfo participant = tblParticipantData.getSelectionModel().getSelectedItem();
		participantDataHolder.setPersonal_info_id(participant.getPersonal_info_id());
		participantDataHolder.setFirst_name(participant.getFirst_name());
		participantDataHolder.setMiddle_name(participant.getMiddle_name());
		participantDataHolder.setLast_name(participant.getLast_name());
		participantDataHolder.setDate_of_birth(participant.getDate_of_birth());
		participantDataHolder.setGender(participant.getGender());
		participantDataHolder.setEthnicity(participant.getEthnicity());
		participantDataHolder.setPhone_number(participant.getPhone_number());
		participantDataHolder.setStreet(participant.getStreet());
		participantDataHolder.setApt_number(participant.getApt_number());
		participantDataHolder.setCity(participant.getCity());
		participantDataHolder.setState(participant.getState());
		participantDataHolder.setZip_code(participant.getZip_code());
		participantDataHolder.setDepartment(participant.getDepartment());
		participantDataHolder.setEmail(participant.getEmail());
		
		new HelperMethods().openEditScene("AddParticipantData");
		loadParticipant();
	}
    	
    @FXML
	private void add() throws IOException, SQLException {
		edit = false;
		new HelperMethods().openEditScene("AddParticipantData");
		loadParticipant();
	}

	@FXML
	private void edit() throws IOException, SQLException {
		edit = true;
		if(tblParticipantData.getSelectionModel().getSelectedItem() != null) 
			getData();
		else
			HelperMethods.alerti("Warning!", "Choose a row!", AlertType.WARNING);
	}
	
	private void delete(int personal_info_id) {
		try {
			ControlDAO.getControlDao().getParticipantDao().deleteParticipantData(personal_info_id);;
			loadParticipant();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
}
