package controller;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXDatePicker;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.MenuButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import utils.HelperMethods;

public class SearchQPController extends VBox {

	@FXML
	private VBox mainVbox;

	@FXML
	private AnchorPane skills, courses, programOfStudy;
	@FXML
	private JFXDatePicker datePicker;

	final MenuButton choicesSkills = new MenuButton("*-Choose Skills-                                     ");
	final MenuButton choicesCourses = new MenuButton("*-Choose Courses-                                   ");
	final MenuButton choicesProgramOfStudy = new MenuButton("*-Choose Program of Study-                   ");

	final List<CheckMenuItem > skillsItems = Arrays.asList( 
			new CheckMenuItem("Fluency in Java") ,
			new CheckMenuItem("Web Design") ,
			new CheckMenuItem ("Bussiness") ,
			new CheckMenuItem ("Communications"),
			new CheckMenuItem("Writing") ,
			new CheckMenuItem("Fluency in HTML") ,
			new CheckMenuItem ("Something") ,
			new CheckMenuItem ("Skill")
			);


	final List<CheckMenuItem > coursesItems = Arrays.asList( 
			new CheckMenuItem("test1") ,
			new CheckMenuItem("test2") ,
			new CheckMenuItem ("test3") ,
			new CheckMenuItem ("test4")
			);


	final List<CheckMenuItem > programOfStudyItems = Arrays.asList( 
			new CheckMenuItem("Computer Programming") ,
			new CheckMenuItem("Program") ,
			new CheckMenuItem ("Program") ,
			new CheckMenuItem ("Program")
			);

	public SearchQPController() {
		FXMLLoader fxml = new FXMLLoader(getClass().getResource("/views/SearchQP.fxml"));

		fxml.setRoot(this);
		fxml.setController(this);
		try {
			fxml.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void initialize() throws SQLException {
		choicesSkills.getItems().addAll(skillsItems);
		choicesCourses.getItems().addAll(coursesItems);
		choicesProgramOfStudy.getItems().addAll(programOfStudyItems);

		skills.getChildren().add(choicesSkills);
		courses.getChildren().add(choicesCourses);
		programOfStudy.getChildren().add(choicesProgramOfStudy);
	}

	@FXML
	private void generate() throws IOException {
		new HelperMethods().openScene("SearchQPTable");
	}
}