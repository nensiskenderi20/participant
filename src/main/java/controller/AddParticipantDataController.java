package controller;

import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXRadioButton;

import dao.ControlDAO;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import model.PersonalInfo;
import utils.HelperMethods;

public class AddParticipantDataController implements Initializable {

    @FXML
    private TextField txtFirstName, txtMiddleName, txtLastName, txtEmail, txtPhoneNumber, txtAddress, txtAptNumber, 
    txtCity, txtState, txtZipCode, txtOther;

    @FXML
    private JFXDatePicker dpBirthdate;

    @FXML
    private JFXRadioButton radioGenderM, radioGenderF, radioAsian, radioHispanic, radioNativeAmerican, 
    radioLatinAmerican, radioOther;
    
    private int participantId = 0;


    @FXML
    private JFXButton btnCancel;
    
    @FXML
    ToggleGroup group1 = new ToggleGroup();
    
    @FXML
    ToggleGroup group2 = new ToggleGroup();
    
    @Override
	public void initialize(URL location, ResourceBundle resources) {
    	//radioGroups() method helps to organize the gender and ethnicity 
    	radioGroups();
    	//we make the phone number, zip ode and apt number to accept only numbers
    	HelperMethods.make_textfield_integer(txtPhoneNumber);
    	HelperMethods.make_textfield_integer(txtZipCode);
    	HelperMethods.make_textfield_integer(txtAptNumber);
    	//if no ethnicity is clicked we select other and type in the value that we want by enabling the txtOther field 
    	group2.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue == radioOther)
            	txtOther.setDisable(false);
            else
            	txtOther.setDisable(true);
            	txtOther.setText("");
        });
    	
    	//if we want to edit a participant we check for this edit boolean value 
    	if(ParticipantDataController.edit == true) {
    		//and we get all the data from the table view
    		participantId = ParticipantDataController.participantDataHolder.getPersonal_info_id();
    		txtFirstName.setText(ParticipantDataController.participantDataHolder.getFirst_name());
    		txtMiddleName.setText(ParticipantDataController.participantDataHolder.getMiddle_name());
    		txtLastName.setText(ParticipantDataController.participantDataHolder.getLast_name());
    		dpBirthdate.setValue(ParticipantDataController.participantDataHolder.getDate_of_birth().toLocalDate());
    		if(ParticipantDataController.participantDataHolder.getGender().equals("F"))
    			group1.selectToggle(radioGenderF);
    		else
    			group1.selectToggle(radioGenderM);
    		
    		txtEmail.setText(ParticipantDataController.participantDataHolder.getEmail());
    		txtPhoneNumber.setText(ParticipantDataController.participantDataHolder.getPhone_number());
    		txtAddress.setText(ParticipantDataController.participantDataHolder.getStreet());
    		txtAptNumber.setText(ParticipantDataController.participantDataHolder.getApt_number() + "");
    		txtCity.setText(ParticipantDataController.participantDataHolder.getCity());
    		txtZipCode.setText(ParticipantDataController.participantDataHolder.getZip_code() + "");
    		if(ParticipantDataController.participantDataHolder.getEthnicity().equals("Asian"))
    			group2.selectToggle(radioAsian);
    		else if(ParticipantDataController.participantDataHolder.getEthnicity().equals("Hispanic"))
    			group2.selectToggle(radioHispanic);
    		else if(ParticipantDataController.participantDataHolder.getEthnicity().equals("Native American"))
    			group2.selectToggle(radioNativeAmerican);
    		else if(ParticipantDataController.participantDataHolder.getEthnicity().equals("Latin American"))
    			group2.selectToggle(radioLatinAmerican);
    		else {
    			group2.selectToggle(radioOther);
    			txtOther.setDisable(false);
    			txtOther.setText(ParticipantDataController.participantDataHolder.getEthnicity());
    		}
    	}
    	
    	else {
    		//else if we want to add a new participant we set the id to 0 because we have no existing id
    		dpBirthdate.setValue(LocalDate.now());
    		participantId = 0;
    	}
    	
    		
	}

    @FXML
    private void cancel() {
    	//here we close the stage since we want to cancel add or edit actions
    	HelperMethods.closeStage(btnCancel);
    }

    @FXML
    private void save() throws SQLException {
    	PersonalInfo participant = new PersonalInfo();
    	participant.setPersonal_info_id(participantId);
    	participant.setFirst_name(txtFirstName.getText());
    	participant.setMiddle_name(txtMiddleName.getText());
    	participant.setLast_name(txtLastName.getText());
    	participant.setDate_of_birth(Date.valueOf(dpBirthdate.getValue()));
    	
    	RadioButton selectedRadioButton = (RadioButton) group1.getSelectedToggle();
    	participant.setGender(selectedRadioButton.getText());
    	
    	participant.setEmail(txtEmail.getText());
    	participant.setPhone_number(txtPhoneNumber.getText());
    	participant.setStreet(txtAddress.getText());
    	participant.setApt_number(Integer.parseInt(txtAptNumber.getText()));
    	participant.setCity(txtCity.getText());
    	participant.setState(txtState.getText());
    	participant.setZip_code(Integer.parseInt(txtZipCode.getText()));
    	
    	RadioButton selectedRadioButton2 = (RadioButton) group2.getSelectedToggle();
    	if(selectedRadioButton2.getText().equals("Other"))
    		participant.setEthnicity(txtOther.getText());
    	else
    		participant.setEthnicity(selectedRadioButton2.getText());
    	//after getting all the data if the participantId is 0 it means that we want to ad
    	if(participantId == 0)
			ControlDAO.getControlDao().getParticipantDao().addParticipantData(participant);
		else
			//if there is an existing id it means that we want to edit so we update the row using the database layer class
			ControlDAO.getControlDao().getParticipantDao().updateParticipantData(participant);
		
		HelperMethods.closeStage(btnCancel);
    }
    
    private void radioGroups() {
    	radioGenderM.setToggleGroup(group1);
    	radioGenderF.setToggleGroup(group1);
    	radioAsian.setToggleGroup(group2);
    	radioHispanic.setToggleGroup(group2); 
    	radioNativeAmerican.setToggleGroup(group2); 
        radioLatinAmerican.setToggleGroup(group2); 
        radioOther.setToggleGroup(group2);
    }
	
}

