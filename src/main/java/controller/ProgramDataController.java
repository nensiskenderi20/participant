package controller;

import java.io.IOException;
import java.sql.SQLException;

import com.jfoenix.controls.JFXAlert;
import com.jfoenix.controls.JFXButton;

import dao.ControlDAO;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Program;
import utils.HelperMethods;

public class ProgramDataController extends VBox {

	@FXML
	private Button btnAdd, btnEdit;

	@FXML
	private TableView<Program> tblProgramData;

	@FXML
	private TableColumn<Program, String> tblcolProgramId, tblcolProgramName, tblcolProgramType, tblcolProgramCredential, tblcolProgramDescription;

	@FXML
	private TableColumn<Program, Program> tblcolDelete;

	private ObservableList<Program> tableViewData = FXCollections.observableArrayList();
	public static boolean edit = false;
	public static Program programDataHolder = new Program();
	
	public ProgramDataController() {
		FXMLLoader fxml = new FXMLLoader(getClass().getResource("/views/ProgramData.fxml"));

		fxml.setRoot(this);
		fxml.setController(this);
		try {
			fxml.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void initialize() throws SQLException {
		loadProgram();
	}
	
	private void loadProgram() throws SQLException {
		tblProgramData.getItems().clear();
		tableViewData.addAll(ControlDAO.getControlDao().getProgramDao().getPrograms());
		
		tblcolProgramId.setCellValueFactory(new PropertyValueFactory<>("program_id"));
		tblcolProgramName.setCellValueFactory(new PropertyValueFactory<>("program_name"));
		tblcolProgramType.setCellValueFactory(new PropertyValueFactory<>("program_type"));
		tblcolProgramCredential.setCellValueFactory(new PropertyValueFactory<>("program_credential"));
		tblcolProgramDescription.setCellValueFactory(new PropertyValueFactory<>("program_description"));
		
		tblcolDelete.setStyle("-fx-alignment:center;");
		tblcolDelete.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
		tblcolDelete.setCellFactory(param -> new TableCell<Program, Program>() {

			Button delete = new Button("");

			protected void updateItem(Program f, boolean empty) {
				super.updateItem(f, empty);

				if (f == null) {
					setGraphic(null);
					return;
				}

				setGraphic(delete);
				HelperMethods.styleDeleteButton(delete);

				delete.setOnMouseClicked(event -> {
					JFXAlert alert = new JFXAlert((Stage) tblProgramData.getScene().getWindow());
					JFXButton cancel = new JFXButton("Cancel");
					cancel.setStyle("-fx-background-color: #DA251E; -fx-text-fill: white;-fx-cursor: hand;");
					JFXButton confirm = new JFXButton("Confirm");
					confirm.setStyle("-fx-background-color: #4A86E8; -fx-text-fill: white;-fx-cursor: hand;");
					HelperMethods.deleteAlert(alert, "Are you sure you want to delete?", confirm, cancel);
					confirm.setOnAction(e -> {
						delete(f.getProgram_id());
						alert.close();
					});
					cancel.setOnAction(e1 -> {
						alert.close();
					});
					HelperMethods.refreshFocusTable(tblProgramData);
				});
			}
		});

		tblProgramData.setItems(tableViewData);
	}
	
	private void getData() throws IOException, SQLException {
		//we use this method if we want to edit because we need the data from the row that we clicked
		//then we use this data t be able to edit that specific row
		Program program = tblProgramData.getSelectionModel().getSelectedItem();
		programDataHolder.setProgram_id(program.getProgram_id());
		programDataHolder.setProgram_name(program.getProgram_name());
		programDataHolder.setProgram_type(program.getProgram_type());
		programDataHolder.setProgram_credential(program.getProgram_credential());
		programDataHolder.setProgram_description(program.getProgram_description());
		
		//after we have the data, we open the AddProgram view
		new HelperMethods().openEditScene("AddProgram");
		//and since we edit a row, we load the table again
		loadProgram();
	}
	
	@FXML
	private void add() throws IOException, SQLException {
		//if we want to add then we set the edit boolean to false, and open the AddProgram view so we can add the data
		edit = false;
		new HelperMethods().openEditScene("AddProgram");
		//and since we add a new row, we load the table again
		loadProgram();
	}

	@FXML
	private void edit() throws IOException, SQLException {
		//if we press the edit button, we set this edit boolean to true
		edit = true;
		//if we dont select the we give the use and alert saying to choose a row, otherwise we get the data of the row we want to edit
		if(tblProgramData.getSelectionModel().getSelectedItem() != null) 
			getData();
		else
			HelperMethods.alerti("Warning!", "Choose a row!", AlertType.WARNING);
	}
	
	private void delete(int program_id) {
		try {
			//we delete the row from database by getting the row id
			ControlDAO.getControlDao().getProgramDao().deleteProgram(program_id);
			//then we load the table view again
			loadProgram();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
