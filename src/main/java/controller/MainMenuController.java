package controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import utils.AutoCompleteTextField;
import utils.Controllers;
import utils.TextSearchBuilder;

public class MainMenuController implements Initializable {


	@FXML private VBox mainVbox;
	
	@FXML private HBox hboxSearchHolder;

	AutoCompleteTextField txtSearch = new AutoCompleteTextField();
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		//here we build a customized textsearch for the main menu welcome panel
		TextSearchBuilder.build_txtSearch(txtSearch,hboxSearchHolder);
	}
	
	@FXML
	private void ccriMetricReporting() {

	}

	@FXML
	private void dtlMetricReporting() {

	}

	@FXML
	private void participantData() {
		//if the participant button is clicked, open the participant view
		Controllers.getParticipantData(mainVbox);
	}

	@FXML
	private void programData() {
		//if the program data is clicked, open the program data view
		Controllers.getProgramData(mainVbox);
	}

	@FXML
	private void resumeWorkshop() {

	}

	@FXML
	private void skills() {
		//if the skills button is clicked, open the skills view
		Controllers.getSkills(mainVbox);
	}
	
	@FXML
	private void search() {
		//same logic for qualified participants
		Controllers.getSearchQP(mainVbox);
	}


}
