package controller;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import dao.ControlDAO;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.Participant;

public class SearchQPTableController implements Initializable{

	@FXML
	private TableView<Participant> tblSearchQP;

	@FXML
	private TableColumn<Participant, String> tblcolParticipantId, tblcolFirstName, tblcolMiddleName, tblcolLastName, 
	tblcolProgram, tblcolSkills, tblcolResume;
	
	@FXML
	private Button btnExportPdf;

	@FXML
	private TableColumn<Participant, String> tblcolGpa;
	
	private ObservableList<Participant> tableViewData = FXCollections.observableArrayList();
	
	public static final int RESUME_COLUMN = 7;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			//first thing we do when this view is open is to load the table view
			loadPersonalInfo();
			//also, if we double click on a resume, we should be able to open this resume in the browser
			clickOnResume(null);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void loadPersonalInfo() throws SQLException {
		tblSearchQP.getItems().clear();
		//here we fill the table view from data with the database
		tableViewData.addAll(ControlDAO.getControlDao().getSearchQPDao().viewQualifiedParticipants());

		tblcolParticipantId.setCellValueFactory(new PropertyValueFactory<>("participant_id"));
		tblcolFirstName.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Participant, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(TableColumn.CellDataFeatures<Participant, String> obj) {
				return new SimpleStringProperty(obj.getValue().getPersonal_info_id().getFirst_name());
			}
		});
		tblcolMiddleName.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Participant, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(TableColumn.CellDataFeatures<Participant, String> obj) {
				return new SimpleStringProperty(obj.getValue().getPersonal_info_id().getMiddle_name());
			}
		});
		tblcolLastName.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Participant, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(TableColumn.CellDataFeatures<Participant, String> obj) {
				return new SimpleStringProperty(obj.getValue().getPersonal_info_id().getLast_name());
			}
		});
		tblcolGpa.setCellValueFactory(new PropertyValueFactory<>("gpa"));
		tblcolResume.setCellValueFactory(new PropertyValueFactory<>("participant_resume"));

		tblSearchQP.setItems(tableViewData);
	}

	@FXML
	private void exportPdf() {
		try {

			Stage stage = (Stage)btnExportPdf.getScene().getWindow();
			//here we open a file chooser so the user can decide where to store the pdf he wants to export
			FileChooser fileChooser = new FileChooser();
			FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PDF (*.PDF, *.pdf)", "*.pdf","*.PDF");
			fileChooser.getExtensionFilters().add(extFilter);

			File file = fileChooser.showSaveDialog(stage);

			//we create the PDF structure
			Document document = new Document(PageSize.A4.rotate(), 5f, 5f, 5f, 5f);
			PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream(file.getAbsolutePath()));

			document.open();

			//we set the exact date and time for this action  
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date date = new Date();
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

			Anchor anchorTarget = new Anchor("Date "+dateFormat.format(date) + " Time " + sdf.format(cal.getTime()));

			Paragraph paragraph1 = new Paragraph();
			paragraph1.setAlignment(Paragraph.ALIGN_RIGHT);
			paragraph1.setSpacingBefore(15);
			paragraph1.setSpacingAfter(15);

			paragraph1.add(anchorTarget);
			document.add(paragraph1);

			Paragraph p2 = new Paragraph("Qualified Participants",FontFactory.getFont(FontFactory.TIMES, 18, Font.BOLD, new CMYKColor(169,169,169,0)));
			p2.setAlignment(Paragraph.ALIGN_CENTER);
			p2.setSpacingBefore(20);		
			document.add(p2);

			//here we fill the pdf table with the same data we want to export, same as in the tableview
			PdfPTable t = new PdfPTable(7);
			t.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			t.setSpacingBefore(30);
			t.setWidthPercentage(95);
			t.setSpacingAfter(5);
			Font bold = new Font(FontFamily.HELVETICA, 14, Font.BOLD);

			Phrase phrase1 = new Phrase("ID", bold);
			PdfPCell c1 = new PdfPCell(phrase1);
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			t.addCell(c1);
			Phrase phrase2 = new Phrase("First Name", bold);
			PdfPCell c2 = new PdfPCell(phrase2);
			c2.setHorizontalAlignment(Element.ALIGN_CENTER);
			t.addCell(c2);
			Phrase phrase3 = new Phrase("Middle Name", bold);
			PdfPCell c3 = new PdfPCell(phrase3);
			c3.setHorizontalAlignment(Element.ALIGN_CENTER);
			t.addCell(c3);
			Phrase phrase4 = new Phrase("Last Name", bold);
			PdfPCell c4 = new PdfPCell(phrase4);
			c4.setHorizontalAlignment(Element.ALIGN_CENTER);
			t.addCell(c4);
			Phrase phrase5 = new Phrase("GPA", bold);
			PdfPCell c5 = new PdfPCell(phrase5);
			c5.setHorizontalAlignment(Element.ALIGN_CENTER);
			t.addCell(c5);
			Phrase phrase6 = new Phrase("Program", bold);
			PdfPCell c6 = new PdfPCell(phrase6);
			c6.setHorizontalAlignment(Element.ALIGN_CENTER);
			t.addCell(c6);
			Phrase phrase7 = new Phrase("Skills", bold);
			PdfPCell c7 = new PdfPCell(phrase7);
			c7.setHorizontalAlignment(Element.ALIGN_CENTER);
			t.addCell(c7);

			//for every row in the table view, add that row to the pdf table too
			for(Participant table_pdf : tableViewData){
				t.addCell(String.valueOf(table_pdf.getParticipant_id()));
				t.addCell(table_pdf.getPersonal_info_id().getFirst_name());
				t.addCell(table_pdf.getPersonal_info_id().getMiddle_name());
				t.addCell(table_pdf.getPersonal_info_id().getLast_name());
				t.addCell(String.valueOf(table_pdf.getGpa()));
				t.addCell("");
				t.addCell("");

			}
			document.add(t);

			PdfPTable table = new PdfPTable(1);
			table.setWidthPercentage(95);
			table.setSpacingBefore(50);
			table.getDefaultCell().setUseAscender(true);
			table.getDefaultCell().setUseDescender(true);
			document.add(table);
			document.close();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
	
	public void clickOnResume(MouseEvent event) {
	    tblSearchQP.setOnMouseClicked(new EventHandler<MouseEvent>() {
	        @Override
	        public void handle(MouseEvent event) {
	        	//if the user clicks twice
	        	if (event.getClickCount() == 2) {
	        		//and he clicks on the resume column
	        	 	if(tblSearchQP.getSelectionModel().getSelectedCells().get(0).getColumn() == RESUME_COLUMN ) {
	        	 		Desktop desktop = Desktop.getDesktop();
	        	 		try {
	        	 			//get the resume pdf file path and open it in your default browser
							desktop.open(new File(System.getProperty("user.dir") + "\\resumes\\" + tblSearchQP.getSelectionModel().getSelectedItem().getParticipant_resume()));
						} catch (IOException e) {
							e.printStackTrace();
						}
	        	 	
	        	 	}
	        		else
	        			return;
	            }
	        }
	    });
	}
	
}